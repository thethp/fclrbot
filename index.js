const Telegraf = require('telegraf');
const config = require('./config');
const bot = new Telegraf(config.botToken);
const firebase = require('firebase-admin');

var serviceAccount = require("./firebaseAccountKey.json");
firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://fclr-3754d.firebaseio.com"
});
var db = firebase.database();
var userDatabase = db.ref('FCLR-Users');

bot.start((ctx) => ctx.replyWithMarkdown('Hello there! If you\'re not an FCLR member, you might be a bit lost. But, if you ARE an FCLR member- welcome! All you need to do here is tell me what cons you work for :) type /mycons followed by the cons you staff.  For example:</br> /mycons MFF, Camp Feral'));

bot.command('mycons', (ctx) => {
    let userId = ctx.message.from.username;
    let text = ctx.message.text;
    if(text == '/mycons' || text == '/mycons ') {
	return ctx.replyWithMarkdown('I\'m sorry, you need to enter in the cons you work after the commmand for example: /mycons MFF, Camp Feral');
    } else {
	userDatabase.update({[userId]: {ConsWorking: text.substr(8)}}); //8 = '/mycons '
    }
});

bot.on('inline_query', (ctx) => {
    let query = ctx.update.inline_query.query.substr(0,1) == '@' ? ctx.update.inline_query.query.substr(1) : ctx.update.inline_query.query;
    userDatabase.orderByValue().once('value', (snap) => {
	let resultsArr = [];
	snap.forEach((data, i) => {
	    console.log(data.val().ConsWorking);
	    if(data.key.toLowerCase().indexOf(query.toLowerCase()) >= 0) {
		resultsArr.push({
		    type: 'article',
		    id: data.key + 'huh',
		    title: data.key + ' - ' + data.val().ConsWorking,
		    input_message_content: {message_text: 'You clicked on one of the search results. That\'s okay, we all make mistakes. Next time, just delete it after you do a search :)'},
		});
	    }
	});
	console.log(resultsArr);
	ctx.answerInlineQuery(resultsArr, {cach_time: 0});
    });
});

bot.on('new_chat_members', (ctx) => {
    ctx.replyWithMarkdown('Welcome to FCLR Mentoring, @' + ctx.update.message.new_chat_member.username  + 'Please introduce yourself.  Additionall set your cons you staff by using the /mycons command [IE: \'/mycons MFF, Camp Feral\'] to see which con someone else works for, type @fclrbot username [IE: \'@fclrbot cptntiller\']. Have a great day!');
});

bot.launch();
